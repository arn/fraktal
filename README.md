# Fraktal
fraktal is a mandelbrot set visualizer. I made it without a specific goal other than to have fun and build a toy I enjoy using.

![example image](sample.png)


## Build
For many Linux environments, fraktal should work out of the box with `cargo run --release`. I know this works on recent versions of KDE/X11/Arch.

I have also been experimenting with NixOS recently, so there is a `shell.nix` which worked for me when trying to run it. After building the binary, you probably need to use `autoPatchelf`.

## Usage

### Controls
The controls are designed for use with a single mouse.

| Action            | Binding                |
| ----------------- | ---------------------- |
| Zoom              | Scroll                 |
| Pan               | Left-click and drag    |
| Change iterations | Right-click and scroll |

The number of iterations are displayed in the titlebar.

### CLI
There are basic CLI flags you can use for some different features I experimented with, but the defaults are what worked best for my enjoyment.

You can see this with `--help`, but here is a dump:
```
Usage: fraktal [OPTIONS]

Options:
      --init-width <INIT_WIDTH>
          Width, in pixels, of the initial window [default: 800]
      --init-height <INIT_HEIGHT>
          Height, in pixels, of the initial window [default: 600]
      --smooth-coloring <SMOOTH_COLORING>
          Enables the smooth coloring feature, which should remove visible banding in the generated image [default: true] [possible values: true, false]
      --histogram-coloring <HISTOGRAM_COLORING>
          Enables the histogram coloring feature. Trades making the coloring stable as iterations increase for making it unstable as the image bounds change [default: false] [possible values: true, false]
      --pane-history-depth <PANE_HISTORY_DEPTH>
          Number of old pane maps to keep in memory. Higher values use more memory, but will produce prettier results when zooming rapidly [default: 10]
  -h, --help
          Print help
```

## Wishlist / future features
* Configurable controls / alternative bindings for keyboard and touchpads.
* Reduce or eliminate third party dependencies. `winit` is a pretty egregious one, though it does provide a good framework for what I was trying to do here.
* Implement arbitrary precision, so we can zoom farther. An early version of this worked, but was extremely slow. See if there is a way to optimize for the problem at hand.
* Colors, instead of black and white.
* 

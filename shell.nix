let
  pkgs = import <nixpkgs> { };
in
pkgs.mkShell {
  name = "fraktal";
  nativeBuildInputs = with pkgs; [
    autoPatchelfHook
    cargo
    clippy
    gcc
    rustc
    rustfmt
    libgcc.lib
  ];

  runtimeDependencies = with pkgs; [
    libxkbcommon
    vulkan-loader
    xorg.libX11
    xorg.libXcursor
    xorg.libXi
  ];
}

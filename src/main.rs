#![deny(clippy::all)]

use clap::Parser;
use error_iter::ErrorIter as _;
use log::error;
use once_cell::sync::Lazy;
use pixels::{Error, Pixels, SurfaceTexture};
use rayon::prelude::*;
use std::collections::LinkedList;
use std::sync::mpsc;
use std::{
    collections::{HashMap, HashSet},
    ops,
};
use thread_priority::*;
use winit::{
    application::ApplicationHandler,
    dpi::LogicalSize,
    event::WindowEvent,
    event_loop::{ActiveEventLoop, EventLoop},
    keyboard::NamedKey,
    window::Window,
};

/// The size of a pane in pixels.
const PANE_PX_SIZE: isize = 256;

/// How much to oversample new panes relative to the current porthole zoom level.
const OVERSAMPLE_FACTOR: f64 = 2.0;

#[derive(Parser)]
#[command()]
struct Args {
    /// Width, in pixels, of the initial window.
    #[arg(long, default_value_t = 800)]
    init_width: usize,

    /// Height, in pixels, of the initial window.
    #[arg(long, default_value_t = 600)]
    init_height: usize,

    /// Enables the smooth coloring feature, which should remove visible banding in the generated image.
    #[arg(long, action = clap::ArgAction::Set, default_value_t = true)]
    smooth_coloring: bool,

    /// Enables the histogram coloring feature. Trades making the coloring stable as iterations increase for making it unstable as the image bounds change.
    #[arg(long, action = clap::ArgAction::Set, default_value_t = false)]
    histogram_coloring: bool,

    /// Number of old pane maps to keep in memory.
    /// Higher values use more memory, but will produce prettier results when zooming rapidly.
    #[arg(long, default_value_t = 10)]
    pane_history_depth: usize,
}
static ARGS: Lazy<Args> = Lazy::new(Args::parse);

pub fn now_monotonic() -> (i64, i64) {
    let mut time = libc::timespec {
        tv_sec: 0,
        tv_nsec: 0,
    };
    let ret = unsafe { libc::clock_gettime(libc::CLOCK_MONOTONIC, &mut time) };
    assert!(ret == 0);
    (time.tv_sec, time.tv_nsec)
}

struct Application {
    pixels: Option<Pixels>,
    window: Option<Window>,
    pixel_mapper: PixelMapper,
    pane_receiver: std::sync::mpsc::Receiver<Pane>,
    porthole_inputs: PortholeInputs,
    porthole_inputs_last: PortholeInputs,
    mouse_x_last: f64,
    mouse_y_last: f64,
    mouse_held: bool,
    control_time_last_s: i64,
    control_time_last_ns: i64,
    rx_panes: usize,
    rx_panes_last: usize,
    fast_pool: rayon::ThreadPool,
}

impl Application {
    fn new(
        porthole_inputs: PortholeInputs,
        pixel_mapper: PixelMapper,
        pane_receiver: std::sync::mpsc::Receiver<Pane>,
    ) -> Self {
        Self {
            pixels: None,
            window: None,
            pixel_mapper,
            pane_receiver,
            porthole_inputs,
            porthole_inputs_last: porthole_inputs,
            mouse_x_last: 0.0,
            mouse_y_last: 0.0,
            mouse_held: false,
            control_time_last_s: 0,
            control_time_last_ns: 0,
            rx_panes: 0,
            rx_panes_last: 0,
            fast_pool: rayon::ThreadPoolBuilder::new().build().unwrap(),
        }
    }
}

impl ApplicationHandler for Application {
    fn resumed(&mut self, event_loop: &ActiveEventLoop) {
        let window_attrs =
            winit::window::Window::default_attributes().with_inner_size(LogicalSize::new(
                self.porthole_inputs.width as u32,
                self.porthole_inputs.height as u32,
            ));

        self.window = Some(event_loop.create_window(window_attrs).unwrap());

        self.pixels = Some({
            let surface_texture = SurfaceTexture::new(
                ARGS.init_width as u32,
                ARGS.init_height as u32,
                self.window.as_ref().unwrap(),
            );
            Pixels::new(
                ARGS.init_width as u32,
                ARGS.init_height as u32,
                surface_texture,
            )
            .unwrap()
        });
    }
    fn window_event(
        &mut self,
        event_loop: &ActiveEventLoop,
        _: winit::window::WindowId,
        event: winit::event::WindowEvent,
    ) {
        let window = match self.window.as_ref() {
            Some(n) => n,
            None => return,
        };
        let pixels = match self.pixels.as_mut() {
            Some(n) => n,
            None => return,
        };

        match event {
            // Render the screen.
            WindowEvent::RedrawRequested => {
                let (control_time_cur_s, control_time_cur_ns) = now_monotonic();
                let elapsed_nanos = (control_time_cur_s - self.control_time_last_s) * 1_000_000_000
                    + (control_time_cur_ns - self.control_time_last_ns);
                self.control_time_last_s = control_time_cur_s;
                self.control_time_last_ns = control_time_cur_ns;
                window.set_title(
                    format!(
                        "{:.3} fps / {} iterations",
                        (1_000_000_000_f64 / elapsed_nanos as f64).to_string(),
                        self.porthole_inputs.max_iterations
                    )
                    .as_str(),
                );

                let window_size = window.inner_size();
                if (self.porthole_inputs.width, self.porthole_inputs.height)
                    != (window_size.width as usize, window_size.height as usize)
                {
                    if let Err(err) = pixels.resize_surface(window_size.width, window_size.height) {
                        log_error("pixels.resize_surface", err);
                        event_loop.exit();
                    }
                    if let Err(err) = pixels.resize_buffer(window_size.width, window_size.height) {
                        log_error("pixels.resize_buffer", err);
                        event_loop.exit();
                    }

                    self.porthole_inputs.width = window_size.width as usize;
                    self.porthole_inputs.height = window_size.height as usize;
                }

                // Color the pixels.
                if self.rx_panes != self.rx_panes_last
                    || self.porthole_inputs_last != self.porthole_inputs
                {
                    self.fast_pool.install(|| {
                        pixels
                            .frame_mut()
                            .par_chunks_exact_mut(4)
                            .enumerate()
                            .for_each(|(index, pixel)| {
                                // Get the corodinate of the porthole, then best-effort color it.
                                let x: isize = (index % self.porthole_inputs.width) as isize;
                                let y: isize = (index / self.porthole_inputs.width) as isize;

                                let coord = transform_coordinates(
                                    x,
                                    y,
                                    self.porthole_inputs.zoom,
                                    self.porthole_inputs.x_bias,
                                    self.porthole_inputs.y_bias,
                                );

                                let calc_pixel = self.pixel_mapper.render_pixel(coord);
                                pixel[0] = calc_pixel[0];
                                pixel[1] = calc_pixel[1];
                                pixel[2] = calc_pixel[2];
                                pixel[3] = calc_pixel[3];
                            });
                        self.porthole_inputs_last = self.porthole_inputs;
                        self.rx_panes_last = self.rx_panes;
                    });
                }

                if let Err(err) = pixels.render() {
                    log_error("pixels.render", err);
                    event_loop.exit();
                    return;
                }
            }
            // Handle changes to user input.
            WindowEvent::KeyboardInput { event, .. } => {
                if event.state == winit::event::ElementState::Pressed
                    && event.logical_key == NamedKey::Escape
                {
                    event_loop.exit();
                    return;
                }
            }
            WindowEvent::MouseInput { state, button, .. } => {
                if button == winit::event::MouseButton::Left {
                    self.mouse_held = state == winit::event::ElementState::Pressed;
                }
            }
            WindowEvent::MouseWheel {
                delta: winit::event::MouseScrollDelta::LineDelta(_, scroll_diff),
                ..
            } => {
                if scroll_diff != 0.0 {
                    if self.mouse_held {
                        // Adjust number of iterations.
                        if scroll_diff > 0.0 {
                            self.porthole_inputs.max_iterations += scroll_diff as usize;
                        } else if (-scroll_diff) as usize >= self.porthole_inputs.max_iterations {
                            self.porthole_inputs.max_iterations = 1;
                        } else {
                            self.porthole_inputs.max_iterations =
                                (self.porthole_inputs.max_iterations as isize
                                    + scroll_diff as isize)
                                    as usize;
                        }
                    } else {
                        // Zoom!
                        let zoom_factor = f64::powf(1.1, scroll_diff as f64);

                        self.porthole_inputs.x_bias -= self.mouse_x_last
                            / (self.porthole_inputs.zoom * zoom_factor)
                            - self.mouse_x_last / self.porthole_inputs.zoom;
                        self.porthole_inputs.y_bias += self.mouse_y_last
                            / (self.porthole_inputs.zoom * zoom_factor)
                            - self.mouse_y_last / self.porthole_inputs.zoom;

                        self.porthole_inputs.zoom *= zoom_factor;
                    }
                }
            }
            WindowEvent::CursorMoved { position, .. } => {
                let dy = position.y - self.mouse_y_last;
                let dx = position.x - self.mouse_x_last;
                self.mouse_y_last = position.y;
                self.mouse_x_last = position.x;

                if self.mouse_held {
                    self.porthole_inputs.x_bias -= dx / self.porthole_inputs.zoom;
                    self.porthole_inputs.y_bias += dy / self.porthole_inputs.zoom;
                }
            }
            WindowEvent::CloseRequested => {
                event_loop.exit();
            }
            _ => (),
        }

        // Determine if we need to update the pin.
        {
            let mut need_pin_update = false;

            need_pin_update |=
                self.porthole_inputs_last.max_iterations != self.porthole_inputs.max_iterations;

            let zoom_ratio = self.pixel_mapper.pane_maps.front().unwrap().pin.zoom
                / (self.porthole_inputs.zoom * OVERSAMPLE_FACTOR);
            need_pin_update |= !(0.5..2.0).contains(&zoom_ratio);

            if need_pin_update {
                self.pixel_mapper.change_pin(Pin {
                    max_iterations: self.porthole_inputs.max_iterations,
                    zoom: self.porthole_inputs.zoom * OVERSAMPLE_FACTOR,
                    x_bias: self.porthole_inputs.x_bias,
                    y_bias: self.porthole_inputs.y_bias,
                });
            }
        }

        // Schedule asynchronous rendering.
        self.pixel_mapper.schedule_rendering(&self.porthole_inputs);

        // Process any received events.
        while let Ok(pane) = self.pane_receiver.try_recv() {
            self.pixel_mapper.learn_pane(pane);
            self.rx_panes += 1;
        }

        window.request_redraw();
    }
}

fn main() -> Result<(), Error> {
    env_logger::init();

    let root_event_loop = EventLoop::new().unwrap();

    let init_zoom = ARGS.init_width as f64 / 3.0;
    let porthole_inputs = PortholeInputs {
        max_iterations: 50,
        x_bias: -0.75 * ARGS.init_width as f64 / init_zoom,
        y_bias: 0.5 * ARGS.init_height as f64 / init_zoom,
        width: ARGS.init_width,
        height: ARGS.init_height,
        zoom: init_zoom,
    };
    let mut porthole_inputs_last = porthole_inputs;

    let cur_pin = Pin {
        max_iterations: porthole_inputs.max_iterations,
        zoom: porthole_inputs.zoom,
        x_bias: porthole_inputs.x_bias,
        y_bias: porthole_inputs.y_bias,
    };

    let pane_collection = PaneCollection {
        pin: cur_pin,
        panes: HashMap::new(),
        pending_renders: HashMap::new(),
    };

    let (sender, receiver) = mpsc::channel();

    let mut pixel_mapper = PixelMapper {
        cur_pin,
        sender,
        pane_maps: LinkedList::new(),
    };
    pixel_mapper.pane_maps.push_front(pane_collection);

    // This is a little hack to trick the loop into rendering the first frame.
    porthole_inputs_last.max_iterations = 0;

    // Set up the threadpools. The global rayon threadpool should be given lowest priority, while we create a custom one for real-time user actions.
    rayon::broadcast(|_| {
        set_current_thread_priority(ThreadPriority::Min).unwrap();
    });

    let mut app = Application::new(porthole_inputs, pixel_mapper, receiver);

    root_event_loop.run_app(&mut app).unwrap();

    Ok(())
}

#[derive(Clone, Copy, PartialEq)]
struct Pin {
    max_iterations: usize,
    zoom: f64,
    x_bias: f64,
    y_bias: f64,
}

#[derive(PartialEq, Clone)]
struct Pane {
    solved: bool,
    iters: Vec<(usize, f64)>,
    pin: Pin,
    px_x_offset: isize,
    px_y_offset: isize,
    width: usize,
    height: usize,
}

impl Pane {
    fn with_pane_inputs(pin: Pin, px_x_offset: isize, px_y_offset: isize) -> Self {
        Self {
            solved: false,
            iters: Vec::new(),
            pin,
            px_x_offset,
            px_y_offset,
            width: PANE_PX_SIZE as usize,
            height: PANE_PX_SIZE as usize,
        }
    }

    fn render(&mut self) {
        self.iters.resize(self.width * self.height, (0, 0.0));
        self.iters
            .iter_mut()
            .enumerate()
            .for_each(|(index, (iter_count, nu))| {
                let x: isize = self.px_x_offset + (index % self.width) as isize;
                let y: isize = self.px_y_offset + (index / self.width) as isize;

                let mut num =
                    transform_coordinates(x, y, self.pin.zoom, self.pin.x_bias, self.pin.y_bias);
                let orig_num = num;

                let mut done_iters = 0;
                for _ in 0..self.pin.max_iterations {
                    let r2 = num.r * num.r;
                    let i2 = num.i * num.i;
                    let ri = num.r * num.i;

                    num = Coord {
                        r: r2 - i2 + orig_num.r,
                        i: 2. * ri + orig_num.i,
                    };

                    let early_break = r2 + i2 > 4.;

                    if early_break {
                        break;
                    }
                    done_iters += 1;
                }

                if ARGS.smooth_coloring && done_iters < self.pin.max_iterations {
                    let log_zn = (num.r * num.r + num.i * num.i).ln() / 2.;
                    *nu = (log_zn / (2_f64).ln()).ln() / (2_f64).ln();
                } else {
                    *nu = 0.0;
                }

                *iter_count = done_iters;
            });
    }
}

#[derive(Clone)]
struct RenderTask {
    signal_sender: mpsc::Sender<bool>,
}

/// A collection of panes which have the same zoom and base coordinate system.
#[derive(Clone)]
struct PaneCollection {
    pin: Pin,
    panes: HashMap<(isize, isize), Pane>,
    pending_renders: HashMap<(isize, isize), RenderTask>,
}

impl PaneCollection {
    fn learn_pane(&mut self, pane: Pane) {
        if self.pin.max_iterations != pane.pin.max_iterations
            || self.pin.zoom != pane.pin.zoom
            || self.pin.x_bias != pane.pin.x_bias
            || self.pin.y_bias != pane.pin.y_bias
        {
            return;
        }

        self.panes
            .insert((pane.px_x_offset, pane.px_y_offset), pane);
    }
}

struct PixelMapper {
    cur_pin: Pin,
    pane_maps: LinkedList<PaneCollection>,
    sender: mpsc::Sender<Pane>,
}

impl PixelMapper {
    fn schedule_rendering(&mut self, porthole_inputs: &PortholeInputs) {
        let cur_pane_map = self.pane_maps.front_mut().unwrap();

        // Get the rectangle of complex coordinates we wish to tile.
        let topleft_coord = transform_coordinates(
            -PANE_PX_SIZE,
            -PANE_PX_SIZE,
            porthole_inputs.zoom,
            porthole_inputs.x_bias,
            porthole_inputs.y_bias,
        );
        let bottomright_coord = transform_coordinates(
            porthole_inputs.width as isize + PANE_PX_SIZE,
            porthole_inputs.height as isize + PANE_PX_SIZE,
            porthole_inputs.zoom,
            porthole_inputs.x_bias,
            porthole_inputs.y_bias,
        );

        let (x_start_raw, y_start_raw) = transform_inverse_coordinates(
            topleft_coord,
            cur_pane_map.pin.zoom,
            cur_pane_map.pin.x_bias,
            cur_pane_map.pin.y_bias,
        );
        let (x_end_raw, y_end_raw) = transform_inverse_coordinates(
            bottomright_coord,
            cur_pane_map.pin.zoom,
            cur_pane_map.pin.x_bias,
            cur_pane_map.pin.y_bias,
        );

        let x_start = x_start_raw - modulo(x_start_raw, PANE_PX_SIZE);
        let y_start = y_start_raw - modulo(y_start_raw, PANE_PX_SIZE);
        let x_end = x_end_raw + modulo(x_end_raw + PANE_PX_SIZE, PANE_PX_SIZE);
        let y_end = y_end_raw + modulo(y_end_raw + PANE_PX_SIZE, PANE_PX_SIZE);

        let mut desired_panes: HashSet<(isize, isize)> = HashSet::new();
        for x in (x_start..x_end).step_by(PANE_PX_SIZE as usize) {
            for y in (y_start..y_end).step_by(PANE_PX_SIZE as usize) {
                desired_panes.insert((x, y));
            }
        }

        // Now figure out which desired panes do not already exist.
        let existing: HashSet<(isize, isize)> = cur_pane_map
            .panes
            .keys()
            .cloned()
            .collect::<HashSet<(isize, isize)>>()
            .union(&cur_pane_map.pending_renders.keys().cloned().collect())
            .cloned()
            .collect();
        let desired_to_render = desired_panes.difference(&existing);
        let desired_to_free = existing.difference(&desired_panes);

        for (x, y) in desired_to_render {
            assert!(modulo(*x, PANE_PX_SIZE) == 0);
            assert!(modulo(*y, PANE_PX_SIZE) == 0);

            let (signal_sender, signal_receiver) = mpsc::channel();

            cur_pane_map
                .pending_renders
                .insert((*x, *y), RenderTask { signal_sender });

            let mut pane = Pane::with_pane_inputs(self.cur_pin, *x, *y);
            let completion_sender = self.sender.clone();
            rayon::spawn(move || {
                // Early abort if we know we don't care about this result.
                if signal_receiver.try_recv().is_ok() {
                    return;
                }

                pane.render();
                completion_sender.send(pane).unwrap();
            });
        }

        for (x, y) in desired_to_free {
            assert!(modulo(*x, PANE_PX_SIZE) == 0);
            assert!(modulo(*y, PANE_PX_SIZE) == 0);

            cur_pane_map.panes.remove(&(*x, *y));
        }
    }

    fn change_pin(&mut self, new_pin: Pin) {
        self.cur_pin = new_pin;

        // Cancel outstanding renders.
        let cur_pane_map = self.pane_maps.front().unwrap();
        for task in cur_pane_map.pending_renders.values() {
            let _ = task.signal_sender.send(true);
        }

        // Add new map.
        let new_pane_map = PaneCollection {
            pin: new_pin,
            panes: HashMap::new(),
            pending_renders: HashMap::new(),
        };
        self.pane_maps.push_front(new_pane_map);

        // Cleanup very old maps.
        while self.pane_maps.len() > ARGS.pane_history_depth {
            self.pane_maps.pop_back();
        }
    }

    fn learn_pane(&mut self, pane: Pane) {
        let pane_map = self.pane_maps.front_mut().unwrap();
        pane_map
            .pending_renders
            .remove(&(pane.px_x_offset, pane.px_y_offset));
        pane_map.learn_pane(pane);
    }
}

fn modulo(x: isize, m: isize) -> isize {
    ((x % m) + m) % m
}

impl PixelMapper {
    /// Transform a given complex coordinate to its best fit pixel from the known panes.
    fn render_pixel(&self, coord: Coord) -> [u8; 4] {
        for pane_map in &self.pane_maps {
            // Normalize the input coordinate to the pane map's coordinate system.
            let (x, y) = transform_inverse_coordinates(
                coord,
                pane_map.pin.zoom,
                pane_map.pin.x_bias,
                pane_map.pin.y_bias,
            );
            let search_x = x - modulo(x, PANE_PX_SIZE);
            let search_y = y - modulo(y, PANE_PX_SIZE);
            let offset_x = modulo(x, PANE_PX_SIZE);
            let offset_y = modulo(y, PANE_PX_SIZE);
            assert!(search_x % PANE_PX_SIZE == 0);
            assert!(search_y % PANE_PX_SIZE == 0);
            assert!(offset_x < PANE_PX_SIZE);
            assert!(offset_y < PANE_PX_SIZE);
            assert!(offset_x >= 0);
            assert!(offset_y >= 0);

            if let Some(pane) = pane_map.panes.get(&(search_x, search_y)) {
                let (iters, nu) = pane.iters[(offset_y * PANE_PX_SIZE + offset_x) as usize];

                let mut color_index = iters as f64;

                // Blended color
                if ARGS.smooth_coloring {
                    color_index += 1.0 - nu;
                }

                let color = (((pane.pin.max_iterations as f64 - color_index)
                    / pane.pin.max_iterations as f64)
                    * 255.0) as u8;

                return [color, color, color, 0xFF];
            }
        }
        [0xFF, 0xFF, 0xFF, 0xFF]
    }
}

fn log_error<E: std::error::Error + 'static>(method_name: &str, err: E) {
    error!("{method_name}() failed: {err}");
    for source in err.sources().skip(1) {
        error!("  Caused by: {source}");
    }
}

#[derive(Clone, Copy)]
struct Coord {
    r: f64,
    i: f64,
}

impl PartialEq for Coord {
    fn eq(&self, other: &Self) -> bool {
        self.r == other.r && self.i == other.i
    }
}

impl ops::Add<Coord> for Coord {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        let r = self.r + other.r;
        let i = self.i + other.i;

        Self { r, i }
    }
}

/// Transforms (x,y) pixel coordinates to a Coord.
fn transform_coordinates(x: isize, y: isize, zoom: f64, x_bias: f64, y_bias: f64) -> Coord {
    let mut r = x as f64;
    let mut i = -(y as f64);

    r /= zoom;
    i /= zoom;

    r += x_bias;
    i += y_bias;

    Coord { r, i }
}

/// Transforms a Coord to (x,y) coordinates.
fn transform_inverse_coordinates(
    coord: Coord,
    zoom: f64,
    x_bias: f64,
    y_bias: f64,
) -> (isize, isize) {
    let mut x_f = coord.r;
    let mut y_f = coord.i;

    x_f -= x_bias;
    y_f -= y_bias;

    x_f *= zoom;
    y_f *= zoom;

    (x_f as isize, -y_f as isize)
}

#[derive(PartialEq, Clone, Copy)]
struct PortholeInputs {
    max_iterations: usize,
    x_bias: f64,
    y_bias: f64,
    width: usize,
    height: usize,
    zoom: f64,
}
